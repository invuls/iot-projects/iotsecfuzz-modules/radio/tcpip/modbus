from setuptools import setup, find_namespace_packages

setup(
    name='communication.tcpip.modbus',
    version='1.0.0',
    description='module for modbus',
    author='Invuls',
    packages=find_namespace_packages(),
    package_data={'isf.communication.tcpip.modbus.resources': ['*', '**/*']},
    zip_safe=False
)
