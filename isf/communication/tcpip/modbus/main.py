from pymodbus.client.sync import ModbusTcpClient
from pymodbus.pdu import ModbusExceptions, ExceptionResponse
from prettytable import PrettyTable
from pymodbus.repl.helper import Result
from pymodbus.payload import BinaryPayloadDecoder, Endian
import re

FORMATTERS = {
    'int8': 'decode_8bit_int',
    'int16': 'decode_16bit_int',
    'int32': 'decode_32bit_int',
    'int64': 'decode_64bit_int',
    'uint8': 'decode_8bit_uint',
    'uint16': 'decode_16bit_uint',
    'uint32': 'decode_32bit_uint',
    'uint64': 'decode_64bit_int',
    'float16': 'decode_16bit_float',
    'float32': 'decode_32bit_float',
    'float64': 'decode_64bit_float',
}

class isf_modbus(object):
	client = None

	def __init__(self):
		super(isf_modbus, self).__init__()


	def isf_read_coils(self, address_register, quantity_read):
		coils = self.client.read_coils(address_register, quantity_read)
		if type(coils) is ExceptionResponse:
			print(ModbusExceptions.decode(coils.exception_code))
			return
		else:
			x = PrettyTable()
			x.field_names = ["Address bit", "Value"]
			for i in range(quantity_read):
				x.add_row([address_register, coils.bits[i]])
				address_register = address_register + 1
			print("Result of read coils:")
			print(x)
			return coils.bits

	def isf_read_discreteinputs(self, address_register, quantity_read):
		discreteInputs = self.client.read_discrete_inputs(address_register, quantity_read)
		if type(discreteInputs) is ExceptionResponse:
			print(ModbusExceptions.decode(discreteInputs.exception_code))
			return
		else:
			x = PrettyTable()
			x.field_names = ["Address bit", "Value"]
			for i in range(quantity_read):
				x.add_row([address_register, discreteInputs.bits[i]])
				address_register = address_register + 1
			print("Result of read discrete inputs:")
			print(x)
			return discreteInputs.bits

	def isf_read_holding_registers(self, address_register, quantity_read, 
		formating, formatter, byte_order, word_order):
		holding_registers = self.client.read_holding_registers(address_register, quantity_read)
		if type(holding_registers) is ExceptionResponse:
			print(ModbusExceptions.decode(holding_registers.exception_code))
			return
		else:
			if formating:
				if byte_order in ['big', 'little']:
					pass
				else:
					print('Wrong byte order! (big/little)')
					return
				if word_order in ['big', 'little']:
					pass
				else:
					print('Wrong word order! (big/little)')
					return
				x = PrettyTable()
				x.field_names = ["Address register", "Value"]
				for i in holding_registers.registers:
					x.add_row([address_register, i])
					address_register = address_register + 1
				print("Result of read holding registers:")
				print(x)
				print("Formated result:")
				result_dict = holding_registers.__dict__
				result_dict['function_code'] = holding_registers.function_code
				format_result = return_result(result_dict).decode(formatter, byte_order, word_order)
				if format_result == 'error':
					return
				elif format_result == 0:
					print("Result = 0, no changes with format: "+formatter+'/'+byte_order+'/'+word_order)
				else:
					x = PrettyTable()
					x.field_names = ["Format", "Formated result"]
					x.add_row([formatter+'/'+byte_order+'/'+word_order, format_result])
					print(x)
					return {'data': holding_registers.registers, 'formatter': formatter}
				return holding_registers.registers
			else:
				x = PrettyTable()
				x.field_names = ["Address register", "Value"]
				for i in holding_registers.registers:
					x.add_row([address_register, i])
					address_register = address_register + 1
				print("Result of read holding registers:")
				print(x)
				return holding_registers.registers

	def isf_read_input_registers(self, address_register, quantity_read, 
		formating, formatter, byte_order, word_order):
		input_registers = self.client.read_input_registers(address_register, quantity_read)
		if type(input_registers) is ExceptionResponse:
			print(ModbusExceptions.decode(input_registers.exception_code))
			return
		else:
			if formating:
				if byte_order in ['big', 'little']:
					pass
				else:
					print('Wrong byte order! (big/little)')
					return
				if word_order in ['big', 'little']:
					pass
				else:
					print('Wrong word order! (big/little)')
					return
				x = PrettyTable()
				x.field_names = ["Address register", "Value"]
				for i in input_registers.registers:
					x.add_row([address_register, i])
					address_register = address_register + 1
				print("Result of read input registers:")
				print(x)
				print("Formated result:")
				result_dict = input_registers.__dict__
				result_dict['function_code'] = input_registers.function_code
				format_result = return_result(result_dict).decode(formatter, byte_order, word_order)
				if format_result == 'error':
					return
				elif format_result == 0:
					print("Result = 0, no changes with format: "+formatter+'/'+byte_order+'/'+word_order)
				else:
					x = PrettyTable()
					x.field_names = ["Format", "Formated result"]
					x.add_row([formatter+'/'+byte_order+'/'+word_order, format_result])
					print(x)
					return {'data': input_registers.registers, 'formatter': formatter}
				return input_registers.registers
			else:
				x = PrettyTable()
				x.field_names = ["Address register", "Value"]
				for i in input_registers.registers:
					x.add_row([address_register, i])
					address_register = address_register + 1
				print("Result of read input registers:")
				print(x)
				return input_registers.registers

	def isf_write_single_coil(self, address_register, value_write):
		print(value_write)
		if value_write in ['True', 'False']:
			if value_write == 'True':
				value_write = True
			else:
				value_write = False
			self.client.write_coil(address_register, value_write)
			coils = self.client.read_coils(address_register, 1)
			if type(coils) is ExceptionResponse:
				print(ModbusExceptions.decode(coils.exception_code))
				return
			else:
				x = PrettyTable()
				x.field_names = ["Address bit", "Value"]
				x.add_row([address_register, coils.bits[0]])
				address_register = address_register + 1
				print("New value:")
				print(x)
		else:
			print('Wrong coil value! Must be True or False')
			return

	def isf_write_single_register(self, address_register, value_write):
		self.client.write_register(address_register, value_write)
		holding_registers = self.client.read_holding_registers(address_register, 1)
		if type(holding_registers) is ExceptionResponse:
			print(ModbusExceptions.decode(coils.exception_code))
			return
		else:
			x = PrettyTable()
			x.field_names = ["Address bit", "Value"]
			x.add_row([address_register, holding_registers.registers[0]])
			address_register = address_register + 1
			print("New value:")
			print(x)
		

	def isf_write_multiple_coils(self, address_register, values_write):
		check_val = True
		for i in values_write:
			if i in [True, False]:
				pass
			else:
				check_val = False
		if check_val:
			self.client.write_coils(address_register, values_write)
			coils = self.client.read_coils(address_register, len(values_write))
			if type(coils) is ExceptionResponse:
				print(ModbusExceptions.decode(coils.exception_code))
				return
			else:
				x = PrettyTable()
				x.field_names = ["Address bit", "Value"]
				for i in range(len(values_write)):
					x.add_row([address_register, coils.bits[i]])
					address_register = address_register + 1
				print("New values:")
				print(x)
		else:
			print('Wrong coil value! Must be True or False')
			return

	def isf_write_multiple_registers(self, address_register, values_write):
		self.client.write_registers(address_register, values_write)
		holding_registers = self.client.read_holding_registers(address_register, len(values_write))
		if type(holding_registers) is ExceptionResponse:
			print(ModbusExceptions.decode(holding_registers.exception_code))
			return
		else:
			x = PrettyTable()
			x.field_names = ["Address bit", "Value"]
			for i in range(len(values_write)):
				x.add_row([address_register, holding_registers.registers[i]])
				address_register = address_register + 1
			print("New values:")
			print(x)

	def validate_values(self, ip, port, function, address_register, 
	quantity_read,formating, formatter, byte_order, word_order,
	 value_write, values_write):
		if function in [1,2,3,4,5,6,15,16]:
			pass
		else:
			print('Wrong function!')
			return False
		if 0<=address_register<65535:
			pass
		else:
			print('Wrong address_register! Must be in range 0<address_register<65535')
			return False
		if 0<quantity_read<2000:
			pass
		else:
			print('Wrong quantity_read! Must be in range 0<quantity_read<2000')
			return False
		return True


class return_result(Result):

	def decode(self, formatters, byte_order='little', word_order='little'):
		if not isinstance(formatters, (list, tuple)):
			formatters = [formatters]


		if self.function_code not in [3, 4, 23]:
			print('Decoder works only for registers!')
			return
		byte_order = (Endian.Little if byte_order.strip().lower() == "little"
					  else Endian.Big)
		word_order = (Endian.Little if word_order.strip().lower() == "little"
					  else Endian.Big)
		decoder = BinaryPayloadDecoder.fromRegisters(self.data.get('registers'),
													 byteorder=byte_order,
													 wordorder=word_order)

		for formatter in formatters:
			formatter = FORMATTERS.get(formatter)
			if not formatter:
				print("Invalid Formatter - {}".format(formatters[0]))
				decoder = 'error'
				return Decoder
			try:
				decoded = getattr(decoder, formatter)()
			except Exception as e:
				print(e)
				decoded = 'error'
			return decoded
		

def run(ip, port, function, address_register, 
	quantity_read,formating, formatter, byte_order, word_order,
	 value_write, values_write):

	modbus = isf_modbus()
	check = modbus.validate_values(ip, port, function, address_register, 
	quantity_read,formating, formatter, byte_order, word_order,
	 value_write, values_write)

	if check:
		modbus.client = ModbusTcpClient(ip, port)
		check_connect = modbus.client.connect()
		if check_connect:
			if function == 1:
				# Read Coils
				modbus.isf_read_coils(address_register, quantity_read)
			elif function == 2:
				# Read Discrete Inputs
				modbus.isf_read_discreteinputs(address_register, quantity_read)
			elif function == 3:
				# Read Holding Registers
				modbus.isf_read_holding_registers(address_register, quantity_read,
				 formating, formatter, byte_order, word_order)
			elif function == 4:
				# Read Input Registers
				modbus.isf_read_input_registers(address_register, quantity_read,
				 formating, formatter, byte_order, word_order)
			elif function == 5:
				# Force Single Coil
				modbus.isf_write_single_coil(address_register, value_write)
			elif function == 6:
				# Preset Single Register
				modbus.isf_write_single_register(address_register, value_write)
			elif function == 15:
				# Force Multiple Coils
				modbus.isf_write_multiple_coils(address_register, values_write)
			elif function == 16:
				# Preset Multiple Registers
				modbus.isf_write_multiple_registers(address_register, values_write)

			modbus.client.close()
		else:
			return